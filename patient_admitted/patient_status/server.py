from flask import Flask, render_template, request
import pickle
# open a file, where you ant to store the data
import pandas as pd
import numpy as np


# create a flask application
app = Flask(__name__)
model = pickle.load(open('logistic_classification_model.pkl', 'rb'))

# route: http method and path
@app.route("/", methods=["GET"])
def root():
    # return "<h1>Welcome to Flask</h1>"
    return render_template("index.html")


# this function will be executed when user enter
# experience and hits the submit button
@app.route("/predict", methods=["POST"])
def predict_admitted_status():
    # get the experience entered by user
   # Patient_admitted = request.form.get("Patient admitted status")
    CT_SCAN = int(request.form["CT_SCAN"])
    corona_result =int(request.form["corona_result"])
    shortness_of_breath = int(request.form["shortness_of_breath"])
    age_60_and_above= int(request.form["age_60_and_above"])

    # predict the user salary
    Patient_admitted = model.predict([[CT_SCAN,corona_result,shortness_of_breath,age_60_and_above]])
    if Patient_admitted == [0]:
        a1=("Not admitted")
    elif Patient_admitted == [1]:
        a1=("Patient admitted to intensive care unit")
    elif Patient_admitted == [2]:
        a1=("Patient admitted to regular ward")
        print(a1)
    elif Patient_admitted == [3]:
        a1=("Patient admitted to semi-intensive unit")
        print(a1)

    return f"<h1>You have entered CT-SCAN value : {CT_SCAN} </h1>" \
           f"<h1>You have entered corona_result : {corona_result} </h1>" \
           f"<h1>You have entered shortness_of_breath : {shortness_of_breath} </h1>" \
           f"<h1>You have entered age_60_and_above : {age_60_and_above} </h1>" \
           f"<h1>Predicted status of patient admitted = {a1} </h1>"



# start the flask web server
app.run(host="0.0.0.0", port=3002, debug=True)
