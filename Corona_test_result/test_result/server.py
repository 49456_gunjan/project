from flask import Flask, render_template, request
import pickle
import os
import pandas as pd
import numpy as np
# open a file, where you ant to store the data



# create a flask application
app = Flask(__name__)
model = pickle.load(open('test_result_status.pkl', 'rb'))

# route: http method and path
@app.route("/", methods=["GET"])
def root():
    # return "<h1>Welcome to Flask</h1>"
    return render_template("index.html")


# this function will be executed when user enter
# experience and hits the submit button
@app.route("/predict", methods=["POST"])
def predict_test_status():
    # get the experience entered by user
   # Patient_admitted = request.form.get("Patient admitted status")
    gender = int(request.form["gender"])
    cough =int(request.form["cough"])
    fever =int(request.form["fever"])
    head_ache = int(request.form["head_ache"])
    sore_throat = int(request.form["sore_throat"])
    shortness_of_breath = int(request.form["shortness_of_breath"])

    # predict the user salary
    test = model.predict([[gender,cough,fever,head_ache,sore_throat,shortness_of_breath]])
    if test == 0:
        a1=("Negative")
    elif test == 1:
        a1=("Positive")

    return f"<h1>Gender  : {gender} </h1>" \
           f"<h1>Symptoms (cough) : {cough} </h1>" \
           f"<h1>Symptoms (fever) : {fever} </h1>" \
           f"<h1>Symptoms (headache)  : {head_ache} </h1>" \
           f"<h1>Symptoms (sore_throat) : {sore_throat} </h1>" \
           f"<h1>Symptoms (breathing problem)  : {shortness_of_breath} </h1>" \
           f"<h2>Predicted status of patient admitted = {a1} </h2>"


# start the flask web server
app.run(host="0.0.0.0", port=3001, debug=True)
